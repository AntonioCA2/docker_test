# Dockerfile

#utiliser l'image ubuntu
FROM ubuntu:22.04

#mettre à jour les paquets et installations nécessaires
RUN apt-get update \
    && apt-get install -y sl cowsay \
    && apt update -y \
    && apt upgrade -y \
    && apt install wget build-essential libncursesw5-dev libssl-dev -y \
    && apt install python3 -y \
    && apt install vim -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV PATH="$PATH:/usr/games"